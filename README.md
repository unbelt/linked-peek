# LinkedPeek

Utility Chrome extension allowing you to list and see messages without mark them as seen.

### The application has two sections:

-   list of unreaded messages and a counter if they are more then one
-   list of already seen messages

> ⚠️ You can only see the last message.
