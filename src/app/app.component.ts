import { Component, OnInit } from '@angular/core';
import { Constants } from './app.constants';
import { IMessage } from './app.models';

@Component({
    selector: 'app-root',
    templateUrl: 'app.html',
    styleUrls: ['app.scss'],
})
export class AppComponent implements OnInit {
    loading: boolean;
    toastr: string;

    unreadMessages: IMessage[] = [];
    seenMessages: IMessage[] = [];

    private messageElements: NodeListOf<Element>;
    private timeout: any;

    ngOnInit(): void {
        this.init();

        this.timeout = setTimeout(() => {
            if (this.messageElements && !this.messageElements.length && !this.toastr) {
                this.toastr = Constants.NO_RESULT_FOUND_MESSAGE;
            }
        }, 2000);
    }

    ngOnDestroy = (): void => clearTimeout(this.timeout);

    private init(): void {
        this.loading = true;

        chrome.tabs.query(
            {
                active: true,
                windowId: chrome.windows.WINDOW_ID_CURRENT,
            },
            (tabs: chrome.tabs.Tab[]) => {
                const currentUrl = tabs[0].url;

                if (currentUrl && currentUrl.toLowerCase().indexOf(Constants.LINKED_IN_URL) > -1) {
                    chrome.tabs.executeScript(
                        {
                            code: `(${this.getDOM})();`,
                        },
                        (results: string[]) => {
                            const parser = new DOMParser();
                            const result = parser.parseFromString(results[0], 'text/html');

                            this.messageElements = result.querySelectorAll(Constants.MESSAGE_ITEM_CLASS);

                            const messages = this.getMessages();
                            this.unreadMessages = messages.filter(
                                (message: IMessage) => message.unreadMessagesCount
                            );
                            this.seenMessages = messages.filter(
                                (message: IMessage) => !message.unreadMessagesCount
                            );

                            this.loading = false;
                        }
                    );
                } else {
                    this.toastr = Constants.NOT_ON_PAGE_ERROR_MESSAGE;
                    this.loading = false;
                }
            }
        );
    }

    private getDOM = (): string => document.body.innerHTML;

    private getMessages(): IMessage[] {
        const messages: IMessage[] = [];

        this.messageElements.forEach((element: Element) => {
            const imageUrl: HTMLImageElement = element.querySelector(Constants.IMAGE_URL_CLASS);
            const name: HTMLElement = element.querySelector(Constants.SENDER_NAME_CLASS);
            const preview: HTMLElement = element.querySelector(Constants.MESSAGE_PREVIEW_CLASS);
            const content: HTMLElement = element.querySelector(Constants.MESSAGE_CONTENT_CLASS);
            const unreadMessages: HTMLElement = element.querySelector(Constants.UNREAD_MESSAGES_CLASS);
            const unreadMessagesCount = unreadMessages && Number(unreadMessages.innerText.trim());

            const message: IMessage = {
                senderImageUrl: imageUrl && imageUrl.src,
                senderName: name && name.innerText.trim(),
                preview: preview && preview.innerText.trim(),
                content: content && content.innerText.trim(),
                hasMore: unreadMessagesCount - 1 > 0,
                unreadMessagesCount,
            };

            messages.push(message);
        });

        return messages;
    }
}
