export class Constants {
    static LINKED_IN_URL = 'linkedin.com';
    static NO_RESULT_FOUND_MESSAGE = 'No messages found!';
    static NOT_ON_PAGE_ERROR_MESSAGE =
        'Not in LinkedIn? Please select LinkedIn tab & expand the Messaging widget';
    static MESSAGE_ITEM_CLASS = '.msg-overlay-list-bubble__convo-card';
    static IMAGE_URL_CLASS = '.presence-entity__image';
    static SENDER_NAME_CLASS = '.msg-conversation-card__participant-names';
    static MESSAGE_PREVIEW_CLASS = '.msg-overlay-list-bubble__message-snippet';
    static MESSAGE_CONTENT_CLASS = '.msg-conversation-card__message-snippet-body';
    static UNREAD_MESSAGES_CLASS = '.msg-conversation-card__unread-count';
}
