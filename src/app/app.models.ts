export interface IMessage {
    senderImageUrl: string;
    senderName: string;
    preview: string;
    hasMore: boolean;
    content?: string;
    unreadMessagesCount?: number;
}
