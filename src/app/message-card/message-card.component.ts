import { Component, Input } from '@angular/core';
import { IMessage } from '../app.models';

@Component({
    selector: 'app-message-card',
    templateUrl: 'message-card.html',
    styleUrls: ['message-card.scss'],
})
export class MessageCardComponent {
    @Input() messages: IMessage[];
}
